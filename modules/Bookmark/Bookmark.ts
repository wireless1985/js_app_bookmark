import { observable, computed , action} from "mobx";
import _ = require("lodash");


export interface BookmarkConfig{
    getFireBase: ()=>Promise<any>
}

export default class Bookmark{

    constructor(config: BookmarkConfig){

        //Пример
        config.getFireBase().then((firebase)=>{
            /*
            let db = firebase.firestore();
            db.collection("users").add({
                first: "Ada",
                last: "Lovelace",
                born: 1815
            })
                .then(function(docRef) {
                    console.log("Document written with ID: ", docRef.id);
                })
                .catch(function(error) {
                    console.error("Error adding document: ", error);
                });
             */
        })

    }


    @observable lastError: boolean|string = false;

    /**
     * ДОБАВЛЕНИЕ И ПРОССМОТР ПАПОК ЗАКЛАДОК
     */

    @observable folders:Array<{
        id: string;
        name: string;
        /**
         * Кол-во закладок в папке
         */
        count: number;

    }> = [];


    @observable newFolder = '';

    /**
     * Инициалицируем данные обо всех папках пользователя
     */
    @action initFolders(){
        this.folders = [];
        return this;
    }


    @action addFolder(){
        let folderName = this.newFolder;
        /*
        Проверка: не добавлять папку, если уже есть папка с таким наименование
        Ограничение: нельзя добавлять более 100 папок
         */

        /*
        После добавления очищаем
         */
        this.newFolder = '';
    }

    /**
     * ДОБАВЛЕНИЕ ЗАКЛАДОК
     */

    @observable newBookmarkText = '';

    @observable newBookmarkHref = '';

    @action addBookmark(){

        let folderId = this.currentFolderId;
        let textOfBookmark = this.newBookmarkText;
        let hrefOfBookmark = this.newBookmarkHref;
        /*
        Ограничение: нельзя добавлять более 500 закладок в одну папку
         */
        /*
        Если folderId не задано (currentFolderIsEmpty()), то создается папка Прочее (если она ранее не была создана) и закладка помещается в эту папку
         */

    }

    @observable currentFolderId: string = '';

    @action setCurrentFolder(folderId){
        this.currentFolderId = folderId;
    }

    @action clearCurrentFolder(){
        this.currentFolderId = '';
        return this;
    }

    @computed currentFolderIsEmpty(): Boolean{
        return true;
    }

    @computed getCurrentFolder(): {id: string; name: string}{
        return {
            id:'',
            name:'',
        };
    }

    /*
    ПРОССМОТР ЗАКЛАДОК В РАЗРЕЗЕ ПАПОК
     */

    /**
     * Инициалицируем данные: 10 последних закладок
     */
    @action initLastBookmark(){
        this.lastBookmarks = [];
    }

    @action init(){
        this.initFolders();
        this.initLastBookmark();
    }

    @observable bookmarks:Array<{
        id: string;
        name: string;
        href: number;
    }> = [];

    @observable lastBookmarks:Array<{
        id: string;
        name: string;
        href: number;
    }> = [];

    @observable filterFolderId: string = '';

    @action setFilterFolder(folderId){
        this.filterFolderId = folderId;
    }

    @action clearFilterFolder(){
        this.filterFolderId = '';
        return this;
    }

    @computed currentFilterIsEmpty(): Boolean{
        return true;
    }

    @computed getFilterFolder(): {id: string; name: string}{
        return {
            id:'',
            name:'',
        };
    }



}