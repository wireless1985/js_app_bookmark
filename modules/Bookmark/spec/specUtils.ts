declare var requirejs;
declare var window;


export default {
    getFireBase: function (): Promise<any> {
        return new Promise(function (resolve) {
            requirejs([
                'https://www.gstatic.com/firebasejs/4.9.0/firebase.js'
            ], function () {
                requirejs(['https://www.gstatic.com/firebasejs/4.9.0/firebase-firestore.js'], function () {
                    let firebaseApp = window.firebase;
                    firebaseApp.initializeApp(window.gfirebaseConfig);
                    resolve(firebaseApp);
                });
            })
        })
    }
};
