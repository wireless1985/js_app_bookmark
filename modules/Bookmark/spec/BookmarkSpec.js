define(["require", "exports", "../Bookmark", "./specUtils"], function (require, exports, Bookmark_1, specUtils_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var instanceOfBookmark = new Bookmark_1.default({
        getFireBase: specUtils_1.default.getFireBase
    });
    describe('Bookmark user story', function () {
        /**
         * User story - описывает сценарий взаимодействия пользователя с программой
         */
        describe('User story 1. Пользователь может создать папку в закладках', function () {
            /**
             * User Action - действие пользователя
             */
            it('User Action: пользователь открывает окно с папками закладок', function () {
                //Инициалицируем данные обо всех папках пользователя
                instanceOfBookmark.initFolders();
            });
            /**
             * UI - описывает данные, которые будет использовать React компонент
             */
            it('UI: отображает все папки и поле ввода наименования новой папки', function () {
                instanceOfBookmark.folders;
                instanceOfBookmark.newFolder;
            });
            it('User Action: пользователь добавляет новую папку', function () {
                instanceOfBookmark.addFolder();
            });
            it('UI: отображает все папки и поле ввода наименования новой папки или результат ошибки', function () {
                instanceOfBookmark.folders;
                instanceOfBookmark.newFolder;
                instanceOfBookmark.lastError;
            });
        });
        describe('User story 2. Пользователь может добавить в закладки элемент', function () {
            it('User Action: пользователь открывает окно с папками', function () {
                //Инициалицируем данные обо всех папках пользователя
                instanceOfBookmark.initFolders();
            });
            it('UI: отображает все папки, а также текущую, текст и ссылка закладки', function () {
                instanceOfBookmark.folders;
                instanceOfBookmark.getCurrentFolder();
            });
            it('User Action: пользователь выбирает папку, в котороую будет добавлять закладку ', function () {
                instanceOfBookmark.setCurrentFolder('folderId');
            });
            it('User Action: пользователь добавляет закладку в папку ', function () {
                instanceOfBookmark.newBookmarkText = 'Text of bookmark';
                instanceOfBookmark.newBookmarkHref = 'https://firebase.google.com/docs/firestore/security/get-started';
                instanceOfBookmark.addBookmark();
            });
        });
        describe('User story 3. Пользователь может увидеть все свои закладки', function () {
            it('User Action: пользователь открывает окно с закладками', function () {
                //Инициалицируем данные обо всех папках пользователя, и 10 последних добавленных закладках
                instanceOfBookmark.init();
            });
            it('UI: отображает все папки, и 10 последних вкладок', function () {
                instanceOfBookmark.folders;
                instanceOfBookmark.lastBookmarks;
            });
            it('User Action: пользователь открывает нужную папку, чтобы увидеть все закладки в ней', function () {
                instanceOfBookmark.setFilterFolder('folderId');
            });
            it('UI: отобразим все закладки в выбранной папке ', function () {
                instanceOfBookmark.getFilterFolder();
                instanceOfBookmark.bookmarks;
            });
            it('User Action: может снять фильтр по папке ', function () {
                instanceOfBookmark.clearFilterFolder();
            });
        });
    });
});
