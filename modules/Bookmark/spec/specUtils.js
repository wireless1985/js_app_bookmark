define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = {
        getFireBase: function () {
            return new Promise(function (resolve) {
                requirejs([
                    'https://www.gstatic.com/firebasejs/4.9.0/firebase.js'
                ], function () {
                    requirejs(['https://www.gstatic.com/firebasejs/4.9.0/firebase-firestore.js'], function () {
                        var firebaseApp = window.firebase;
                        firebaseApp.initializeApp(window.gfirebaseConfig);
                        resolve(firebaseApp);
                    });
                });
            });
        }
    };
});
