import Bookmark from "../Bookmark";
import {when, autorun} from "mobx";
import specUtils from "./specUtils";

//todo Для каждого теста создается новый пользователь, для создания пользователя и авторизации использовать JWT
let instanceOfBookmark = new Bookmark({
    getFireBase: specUtils.getFireBase
});

describe('Bookmark user story',function(){


    /**
     * User story - описывает сценарий взаимодействия пользователя с программой
     */
    describe('User story 1. Пользователь может создать папку в закладках',function(){

        /**
         * User Action - действие пользователя
         */
        it('User Action: пользователь открывает окно с папками закладок', ()=>{

            //Инициалицируем данные обо всех папках пользователя
            instanceOfBookmark.initFolders();

        });

        /**
         * UI - описывает данные, которые будет использовать React компонент
         */
        it('UI: отображает все папки и поле ввода наименования новой папки', ()=>{

            instanceOfBookmark.folders;
            instanceOfBookmark.newFolder;

        });

        it('User Action: пользователь добавляет новую папку', ()=>{

            instanceOfBookmark.addFolder();

        });

        it('UI: отображает все папки и поле ввода наименования новой папки или результат ошибки', ()=>{

            instanceOfBookmark.folders;
            instanceOfBookmark.newFolder;
            instanceOfBookmark.lastError;

        });

    });

    describe('User story 2. Пользователь может добавить в закладки элемент',function(){

        it('User Action: пользователь открывает окно с папками', ()=>{

            //Инициалицируем данные обо всех папках пользователя
            instanceOfBookmark.initFolders();

        });

        it('UI: отображает все папки, а также текущую, текст и ссылка закладки', ()=>{

            instanceOfBookmark.folders;
            instanceOfBookmark.getCurrentFolder();

        });

        it('User Action: пользователь выбирает папку, в котороую будет добавлять закладку ', ()=>{

            instanceOfBookmark.setCurrentFolder('folderId');

        });

        it('User Action: пользователь добавляет закладку в папку ', ()=>{

            instanceOfBookmark.newBookmarkText = 'Text of bookmark';
            instanceOfBookmark.newBookmarkHref = 'https://firebase.google.com/docs/firestore/security/get-started';
            instanceOfBookmark.addBookmark();

        });

    });


    describe('User story 3. Пользователь может увидеть все свои закладки',function(){

        it('User Action: пользователь открывает окно с закладками', ()=>{

            //Инициалицируем данные обо всех папках пользователя, и 10 последних добавленных закладках
            instanceOfBookmark.init();

        });

        it('UI: отображает все папки, и 10 последних вкладок', ()=>{

            instanceOfBookmark.folders;
            instanceOfBookmark.lastBookmarks;

        });

        it('User Action: пользователь открывает нужную папку, чтобы увидеть все закладки в ней', ()=>{

            instanceOfBookmark.setFilterFolder('folderId');

        });

        it('UI: отобразим все закладки в выбранной папке ', ()=>{

            instanceOfBookmark.getFilterFolder();
            instanceOfBookmark.bookmarks;

        });

        it('User Action: может снять фильтр по папке ', ()=>{

            instanceOfBookmark.clearFilterFolder();

        });

    });



});