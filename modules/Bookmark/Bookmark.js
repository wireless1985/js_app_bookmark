var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
define(["require", "exports", "mobx"], function (require, exports, mobx_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Bookmark = /** @class */ (function () {
        function Bookmark(config) {
            this.lastError = false;
            /**
             * ДОБАВЛЕНИЕ И ПРОССМОТР ПАПОК ЗАКЛАДОК
             */
            this.folders = [];
            this.newFolder = '';
            /**
             * ДОБАВЛЕНИЕ ЗАКЛАДОК
             */
            this.newBookmarkText = '';
            this.newBookmarkHref = '';
            this.currentFolderId = '';
            this.bookmarks = [];
            this.lastBookmarks = [];
            this.filterFolderId = '';
            //Пример
            config.getFireBase().then(function (firebase) {
                /*
                let db = firebase.firestore();
                db.collection("users").add({
                    first: "Ada",
                    last: "Lovelace",
                    born: 1815
                })
                    .then(function(docRef) {
                        console.log("Document written with ID: ", docRef.id);
                    })
                    .catch(function(error) {
                        console.error("Error adding document: ", error);
                    });
                 */
            });
        }
        /**
         * Инициалицируем данные обо всех папках пользователя
         */
        Bookmark.prototype.initFolders = function () {
            this.folders = [];
            return this;
        };
        Bookmark.prototype.addFolder = function () {
            var folderName = this.newFolder;
            /*
            Проверка: не добавлять папку, если уже есть папка с таким наименование
            Ограничение: нельзя добавлять более 100 папок
             */
            /*
            После добавления очищаем
             */
            this.newFolder = '';
        };
        Bookmark.prototype.addBookmark = function () {
            var folderId = this.currentFolderId;
            var textOfBookmark = this.newBookmarkText;
            var hrefOfBookmark = this.newBookmarkHref;
            /*
            Ограничение: нельзя добавлять более 500 закладок в одну папку
             */
            /*
            Если folderId не задано (currentFolderIsEmpty()), то создается папка Прочее (если она ранее не была создана) и закладка помещается в эту папку
             */
        };
        Bookmark.prototype.setCurrentFolder = function (folderId) {
            this.currentFolderId = folderId;
        };
        Bookmark.prototype.clearCurrentFolder = function () {
            this.currentFolderId = '';
            return this;
        };
        Bookmark.prototype.currentFolderIsEmpty = function () {
            return true;
        };
        Bookmark.prototype.getCurrentFolder = function () {
            return {
                id: '',
                name: '',
            };
        };
        /*
        ПРОССМОТР ЗАКЛАДОК В РАЗРЕЗЕ ПАПОК
         */
        /**
         * Инициалицируем данные: 10 последних закладок
         */
        Bookmark.prototype.initLastBookmark = function () {
            this.lastBookmarks = [];
        };
        Bookmark.prototype.init = function () {
            this.initFolders();
            this.initLastBookmark();
        };
        Bookmark.prototype.setFilterFolder = function (folderId) {
            this.filterFolderId = folderId;
        };
        Bookmark.prototype.clearFilterFolder = function () {
            this.filterFolderId = '';
            return this;
        };
        Bookmark.prototype.currentFilterIsEmpty = function () {
            return true;
        };
        Bookmark.prototype.getFilterFolder = function () {
            return {
                id: '',
                name: '',
            };
        };
        __decorate([
            mobx_1.observable
        ], Bookmark.prototype, "lastError", void 0);
        __decorate([
            mobx_1.observable
        ], Bookmark.prototype, "folders", void 0);
        __decorate([
            mobx_1.observable
        ], Bookmark.prototype, "newFolder", void 0);
        __decorate([
            mobx_1.action
        ], Bookmark.prototype, "initFolders", null);
        __decorate([
            mobx_1.action
        ], Bookmark.prototype, "addFolder", null);
        __decorate([
            mobx_1.observable
        ], Bookmark.prototype, "newBookmarkText", void 0);
        __decorate([
            mobx_1.observable
        ], Bookmark.prototype, "newBookmarkHref", void 0);
        __decorate([
            mobx_1.action
        ], Bookmark.prototype, "addBookmark", null);
        __decorate([
            mobx_1.observable
        ], Bookmark.prototype, "currentFolderId", void 0);
        __decorate([
            mobx_1.action
        ], Bookmark.prototype, "setCurrentFolder", null);
        __decorate([
            mobx_1.action
        ], Bookmark.prototype, "clearCurrentFolder", null);
        __decorate([
            mobx_1.computed
        ], Bookmark.prototype, "currentFolderIsEmpty", null);
        __decorate([
            mobx_1.computed
        ], Bookmark.prototype, "getCurrentFolder", null);
        __decorate([
            mobx_1.action
        ], Bookmark.prototype, "initLastBookmark", null);
        __decorate([
            mobx_1.action
        ], Bookmark.prototype, "init", null);
        __decorate([
            mobx_1.observable
        ], Bookmark.prototype, "bookmarks", void 0);
        __decorate([
            mobx_1.observable
        ], Bookmark.prototype, "lastBookmarks", void 0);
        __decorate([
            mobx_1.observable
        ], Bookmark.prototype, "filterFolderId", void 0);
        __decorate([
            mobx_1.action
        ], Bookmark.prototype, "setFilterFolder", null);
        __decorate([
            mobx_1.action
        ], Bookmark.prototype, "clearFilterFolder", null);
        __decorate([
            mobx_1.computed
        ], Bookmark.prototype, "currentFilterIsEmpty", null);
        __decorate([
            mobx_1.computed
        ], Bookmark.prototype, "getFilterFolder", null);
        return Bookmark;
    }());
    exports.default = Bookmark;
});
