var __isNode__ = false;
if (typeof module !== 'undefined' && module.exports) {
    __isNode__ = true;
}

var __defaultConfig__ = {
    requirejs:{
        baseUrl: '.',
        urlArgs: ''
    }
};

var __globalConfig =  (__isNode__?__defaultConfig__:window.mzrConfig||__defaultConfig__);

var _require_config_ = {

    baseUrl : __globalConfig.requirejs.baseUrl,

    urlArgs : (__globalConfig.requirejs.urlArgs||""),

    waitSeconds : 60,

    paths: {

        'prop-types': 'modules/polyfill/propTypes',
        bootstrap: "appDiary/bootstrap/bootstrapV2",
        lib: "lib",
        components: "components",
        vendor: "vendor",
        node_modules: "node_modules",
        moment: 'vendor/moment/min/moment.min',//'lib/Core/moment',
        //momentru: 'vendor/moment/locale/ru',
        momentru: 'modules/moment/ru',
        DateRange: 'vendor/moment-range/dist/moment-range.min',
        'localforage' : 'node_modules/localforage/dist/localforage.min',
        "uikit": 'ui/dist/js/uikit',
        "uikit-tooltip": "ui/dist/js/components/tooltip",
        "uikit-lightbox": "ui/dist/js/components/lightbox",
        'scrollMonitor':"vendor/scrollMonitor/scrollMonitor",
        "uikitjs": 'ui/dist/js',
        'uikitdir': 'ui/dist',
        "uimzr":'ui/uimzr',

        'amcharts'          : 'vendor/amcharts/amcharts',
        'amcharts.funnel'   : 'vendor/amcharts/funnel',
        'amcharts.gauge'    : 'vendor/amcharts/gauge',
        'amcharts.pie'      : 'vendor/amcharts/pie',
        'amcharts.radar'    : 'vendor/amcharts/radar',
        'amcharts.serial'   : 'vendor/amcharts/serial',
        'amcharts.xy'       : 'vendor/amcharts/xy',
        'amcharts.ru'       : 'vendor/amcharts/lang/ru',
        'amcharts.responsive'       : 'vendor/amcharts/plugins/responsive/responsive.min',

        'lodash': 'vendor/lodash/lodash.min',
        'ServerDataSportDiary': 'service/ServerDataSportDiary',
        'fabric': 'vendor/fabric/dist/fabric.require',
        'ServerDataMeasuring': 'service/ServerDataMeasuring',
        'ServerDataCourse':'service/ServerDataCourse',


        'mzr-sportdiary':  'modules/SportDiary/index',
        'mzr-fooddiary':  'modules/FoodDiary/lib/index',
        'mzr-food':'modules/Food/lib/Entry',
        'FoodServiceBrowser': 'modules/Food/serviceBrowser/FoodServiceBrowser',
        "ControllersCommon": "controller/Account/entry",

        'react': "node_modules/react/dist/react",
        'react-dom': 'node_modules/react-dom/dist/react-dom',
        'classnames': 'modules/classnames/index',
        'mobx': 'node_modules/mobx/lib/mobx.umd',
        'mobx-react': 'node_modules/mobx-react/index',

        'css': 'vendor/require-css/css.min'

    },

    config: {
        moment: {
            noGlobal: true
        },
        "uikit": {
            "base": "ui/dist/js"
        }
    },


    // Define dependencies
    shim: {


        'uikit' : {
            deps : ['jquery']
        },
        'amcharts.funnel'   : {
            deps: ['amcharts'],
            exports: 'AmCharts',
            init: function() {
                AmCharts.isReady = true;
            }
        },
        'amcharts.gauge'    : {
            deps: ['amcharts'],
            exports: 'AmCharts',
            init: function() {
                AmCharts.isReady = true;
            }
        },
        'amcharts.pie'      : {
            deps: ['amcharts'],
            exports: 'AmCharts',
            init: function() {
                AmCharts.isReady = true;
            }
        },
        'amcharts.radar'    : {
            deps: ['amcharts'],
            exports: 'AmCharts',
            init: function() {
                AmCharts.isReady = true;
            }
        },
        'amcharts.serial'   : {
            deps: ['amcharts'],
            exports: 'AmCharts',
            init: function() {
                AmCharts.isReady = true;
            }
        },
        'amcharts.xy'       : {
            deps: ['amcharts'],
            exports: 'AmCharts',
            init: function() {
                AmCharts.isReady = true;
            }
        },

        'amcharts.ru'       : {
            deps: ['amcharts'],
            init: function() {
                AmCharts.isReady = true;
            }
        },

        'amcharts.responsive' : {
            deps: ['amcharts'],
            init: function() {
                AmCharts.isReady = true;
            }
        }


    },

    map: {
        '*': {
            'css': 'css'
        }
    }

};

if(__isNode__===false){
    window._require_config_ = _require_config_;
    if (window.jQuery) {
        define('jquery', [], function() {
            return jQuery;
        });
    }else{
        _require_config_.paths.jquery = 'node_modules/jquery/dist/jquery.min';
    }

    if (typeof __bundles__ !== 'undefined') {
        _require_config_.bundles = __bundles__;
    }
    window._require_config_ = _require_config_;
    if(typeof requirejs !== 'undefined'){
        requirejs.config(_require_config_);
    }
}else{
    _require_config_.paths.jquery = 'node_modules/jquery/dist/jquery.min';
    module.exports = _require_config_;
}