var
    gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    path        = require('path'),
    Promise     = require('promise');

var ts = require("gulp-typescript");

var watchFoldersForTypeScript = ['modules/**/*.ts','modules/**/*.tsx'];
var tsProject = ts.createProject('tsconfig.json');

gulp.task('typescript', function() {


    var tsResult = gulp.src(watchFoldersForTypeScript, {base: "./"})
        .pipe(tsProject());

    return tsResult.js.pipe(gulp.dest("./"));
});

gulp.task('watch-typescript', ['typescript'], function() {

    gulp.watch(watchFoldersForTypeScript, ['typescript']);

});